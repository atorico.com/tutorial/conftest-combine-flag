package k8s.namespace

import data.stage

deny_namespace_incorrect_prefix[msg] {
    manifest = input[idx].contents
    file = input[idx].path
    stageName := data.stage.name
    stagePrefix := concat("",[ stageName, "-"])
    not startswith(manifest.metadata.namespace, stagePrefix)
    msg := sprintf("Namespace %s must be prefixed with stage prefix: %s - FILE: %s", [ manifest.metadata.namespace, stagePrefix, file])
}
