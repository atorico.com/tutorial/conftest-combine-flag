package k8s.kind.deployment

import data.stage

deny_deployment_missing_label[msg] {
    manifest = input[_].contents
    file = input[_].path
    name := manifest.metadata.name
    is_deployment
    required := data.stage.required_labels["Deployment"][_]
    not has_key(manifest.metadata.labels, required)
    msg := sprintf("Deployment %s must be qualified with a label: %s - FILE: %s", [ name , required, file ])

}


is_deployment {
   input[_].contents.kind == "Deployment"
}

has_key(x, k) { 
   _ = x[k] 
}
