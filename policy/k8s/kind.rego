package k8s.kind

import data.stage

deny_kind_is_not_allowed[msg] {
    manifest = input[idx].contents
    file = input[idx].path
    kind := manifest.kind
    all({
        allowed | allowed := data.stage.allowed_kinds[_]
        allowed == kind
    })
    msg := sprintf("Kind %s is not one of the allowed kinds: %s - FILE: %s", [ kind, data.stage.allowed_kinds, file ])
}
